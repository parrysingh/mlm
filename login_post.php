<?php
require_once('not_in.php');
require_once('conn.php');

if($_POST['submit']) {
  $refer_id = $_POST['refer_id'];
  $password = $_POST['password'];

  $query = $db->prepare('SELECT id, password FROM users WHERE refer_id = ?');
  $query->bind_param('s', $refer_id);
  $query->execute();
  $query->bind_result($id, $db_password);
  $query->fetch();
  $query->close();

  if($id) {
    if(password_verify($password, $db_password)) {
      $_SESSION['user'] = $id;
      header('Location: /');
    } else {
      echo "Password doesn't match";
    }
  } else {
    echo "User not found";
  }
}
?>
