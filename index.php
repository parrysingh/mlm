<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Welcome</title>
  </head>
  <body>
    <?php
    if(isset($_SESSION['user'])) {
      ?>
      <a href="/logout.php">Logout</a>
      <?php
    } else {
      ?>
      <a href="/register.php">Register</a> <br>
      <a href="/login.php">Login</a>
      <?php
    }
    ?>
  </body>
</html>
