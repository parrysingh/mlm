<?php
require_once('in.php');
require_once('conn.php');
if(isset($_GET['page'])) {
  $page = $_GET['page'];
} else {
  $page = 1;
}

$limit = 5;
$offset = ($page - 1) * $limit;

$users = $db->prepare("SELECT id, name, sponsor_id, status FROM users ORDER BY created_on ASC LIMIT ? OFFSET ?");
$users->bind_param('ii', $limit, $offset);
$users->execute();
$users->bind_result($id, $name, $sponsor_id, $status);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Users Single Tree</title>
  </head>
  <body>
    <style media="screen">
      .user1 {
        background: green;
        border-radius: 50%;
        width: 200px;
        padding: 2em;
        box-sizing: border-box;
        text-align: center;
      }

      .user0 {
        background: red;
        border-radius: 50%;
        width: 200px;
        padding: 2em;
        box-sizing: border-box;
        text-align: center;
      }
      .user1:before {
        content:'';
        height:0;
        margin:100px 0 0 0;
        display:inline-block;
        vertical-align: middle;
      }

      .user0:before {
        content:'';
        height:0;
        margin:100px 0 0 0;
        display:inline-block;
        vertical-align: middle;
      }

      a span { display: inline-block;  vertical-align: middle;}
    </style>
    <?php
      while($users->fetch()) {
        ?>
          <p>
            <a href="" class="user<?= $status ?>"><span><?= $name ?></span></a>
          </p>
        <?php
      }
    ?>
    <a href="tree.php?page=<?= $page + 1 ?>">Next</a> - <a href="tree.php?page=<?= $page - 1 ?>">Previous</a>
  </body>
</html>
