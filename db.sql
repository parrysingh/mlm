-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema mlm
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mlm
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mlm` DEFAULT CHARACTER SET utf8 ;
USE `mlm` ;

-- -----------------------------------------------------
-- Table `mlm`.`packages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mlm`.`packages` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `created_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mlm`.`pins`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mlm`.`pins` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pin` VARCHAR(6) NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `package_id` INT NOT NULL,
  `created_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `pin` (`pin` ASC),
  INDEX `fk_pins_packages_idx` (`package_id` ASC),
  CONSTRAINT `fk_pins_packages`
    FOREIGN KEY (`package_id`)
    REFERENCES `mlm`.`packages` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mlm`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mlm`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `sponsor_id` VARCHAR(45) NOT NULL,
  `refer_id` VARCHAR(45) NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `created_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
