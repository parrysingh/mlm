<?php
require_once('not_in.php');
require_once('conn.php');

if($_POST['submit']) {
  $name = $_POST['name'];
  $password = $_POST['password'];
  $sponsor_id = $_POST['sponsor_id'];
  $pin_post = $_POST['pin'];

  $query = $db->prepare("SELECT id FROM users WHERE refer_id = ?");
  $query->bind_param("s", $sponsor_id);
  $query->execute();
  $query->bind_result($s_id);
  $query->fetch();
  $query->close();

  $pin = $db->prepare("SELECT status FROM pins WHERE pin = ?");
  $pin->bind_param('s', $pin_post);
  $pin->execute();
  $pin->bind_result($pin_status);
  $pin->fetch();
  $pin->close();

  if($s_id && $pin_status === 0) {

    $refer_id = $pin_post;

    $pin = $db->prepare("UPDATE pins SET status = true WHERE pin = ?");
    $pin->bind_param('s', $refer_id);
    $pin->execute();
    $pin->close();

    $user = $db->prepare("INSERT INTO users(name, password, sponsor_id, refer_id) VALUES(?, ?, ?, ?)");
    $user->bind_param("ssss", $name, password_hash($password, PASSWORD_DEFAULT), $sponsor_id, $refer_id);
    if($user->execute()) {
      $user->close();
      $check_status = $db->prepare("SELECT COUNT(id) FROM users WHERE sponsor_id = ?");
      $check_status->bind_param("s", $sponsor_id);
      $check_status->execute();
      $check_status->bind_result($user_count);
      $check_status->fetch();
      $check_status->close();
      if($user_count >= 3) {
        $green = $db->prepare("UPDATE users SET status = 1 WHERE refer_id = ?");
        $green->bind_param('s', $sponsor_id);
        $green->execute();
        header('Location: /');
      }
    } else {
      echo "There was some error " . $user->error();
    }
  } else {
    var_dump($pin_status);
    echo "Sponsor not present or pin wrong/used";
  }
}


?>
