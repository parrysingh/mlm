<?php
require_once('../in.php');
require_once('../conn.php');
$p = $db->prepare("SELECT id, name FROM packages");
$p->execute();
$p->bind_result($id, $name);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Create Pin</title>
  </head>
  <body>
    <form action="create_pin_post.php" method="post">
      <input type="number" name="quantity"> <br>

      <select name="package_id">
        <?php while($p->fetch()) {
          ?>
          <option value="<?= $id ?>"><?= $name ?></option>
          <?php
        }
        ?>
      </select> <br>
      <input type="submit" name="submit" value="Create Pins">


    </form>
  </body>
</html>
