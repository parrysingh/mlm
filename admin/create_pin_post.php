<?php
require_once('../in.php');
require_once('../conn.php');
if(isset($_POST['submit'])) {
  $quantity = $_POST['quantity'];
  $package_id = $_POST['package_id'];

  $shouldFinish = true; $i = 0;

  while($shouldFinish) {
    $seed = str_split('0123456789');
    shuffle($seed);
    $pin = '';
    foreach (array_rand($seed, 5) as $k) {
      $pin .= $seed[$k];
    }

    $q = $db->prepare("INSERT INTO pins(pin, package_id) VALUES(?, ?)");
    $q->bind_param("si", $pin, $package_id);
    if($q->execute()) {
      $i++;
    }

    if($i == $quantity) {
      $shouldFinish = false;
    }
  }
  header('Location: /admin/pins.php');
}
?>
