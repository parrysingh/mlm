<?php
require_once('../in.php');
require_once('../conn.php');

$pins = $db->prepare("SELECT pi.id, pi.pin, pi.status, pa.name FROM pins pi JOIN packages pa ON pi.package_id = pa.id");
$pins->execute();
$pins->bind_result($id, $pin, $status, $name)
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Pins</title>
  </head>
  <body>
    <a href="create_pin.php">Create Pin</a>
    <table border="1">
      <tr>
        <th>ID</th>
        <th>Pin</th>
        <th>Package</th>
        <th>Status</th>
      </tr>
      <?php
        while($pins->fetch()) {
          ?>
          <tr>
            <td><?= $id ?></td>
            <td><?= $pin ?></td>
            <td><?= $name ?></td>
            <td><?= $status ? 'Used' : 'Unused' ?></td>
          </tr>
          <?php
        }
      ?>
    </table>
  </body>
</html>
